FROM debian:stable-slim
MAINTAINER ypk@m0o.eu
RUN echo postfix postfix/mailname string localdocker | debconf-set-selections;
RUN echo postfix postfix/main_mailer_type string 'No configuration' | debconf-set-selections;
RUN \
  apt-get update && \
  apt-get -y --no-install-recommends install \
    procps \
    postfix \
    libsasl2-modules \
    ca-certificates \
    rsyslog && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

COPY main.cf /etc/postfix/main.cf

# Default config:
# Open relay, trust docker links for firewalling.
# Try to use TLS when sending to other smtp servers.
# No TLS for connecting clients, trust docker network to be safe
ENV \
  POSTFIX_myhostname=localhost \
  POSTFIX_mydestination=localhost \
  POSTFIX_mynetworks=0.0.0.0/0 \
  POSTFIX_smtp_tls_security_level=may \
  POSTFIX_smtpd_tls_security_level=none
COPY rsyslog.conf /etc/rsyslog.conf
COPY run /root/
VOLUME ["/var/lib/postfix", "/var/mail", "/var/spool/postfix"]
EXPOSE 25
CMD ["/root/run"]
