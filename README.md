# postfix-smarthost-relay
Forked from https://hub.docker.com/r/mwader/postfix-relay without dkim and only sender based auth support. Tested with Gmail.

Aimed to route based on From email header to the right SMTP server. Can be plugged into any mail system as relay.

## Usage
`docker pull yannk/postfix-smarthost-relay` or clone/build it yourself.
 

All postfix [configuration options](http://www.postfix.org/postconf.5.html)
can be set using `POSTFIX_<name>` environment
variables. See [Dockerfile](Dockerfile) for default configuration. You probably
want to set `POSTFIX_myhostname` (the FQDN used by 220/HELO).

Note that `POSTFIX_myhostname` will change the postfix option
[myhostname](http://www.postfix.org/postconf.5.html#myhostname).

#### Using docker run
```
docker run -e POSTFIX_myhostname=smtp.domain.tld -p 25:25 -v `pwd`/config/sasl_passwd:/etc/postfix/sasl_passwd -v `pwd`/config/sender_relay:/etc/postfix/sender_relay yannk/postfix-smarthost-relay
```
Only when /etc/postfix/sasl_passwd and /etc/postfix/sender_relay is accessible in the container, this feature will be activated.


#### Using docker-compose
```
app:
  # use hostname "smtp-relay" as SMTP server

smtp-relay:
  image: yannk/postfix-smarthost-relay
  restart: always
  environment:
    - POSTFIX_myhostname=smtp.domain.tld
  volumes:
    - sasl_passwd:/etc/postfix/sasl_passwd
    - sender_relay:/etc/postfix/sender_relay
```

